let givenData=require('./3-arrays-vitamins.cjs');

function groupVitamins(array){
    let obtainedData=array.reduce((initialObject,object)=>{
        let vitaminArray=object.contains.split(",")

        let newArray=vitaminArray.map((element)=>{
            if(!initialObject[element.trim()]){
                initialObject[element.trim()]=[object.name];
            }else{
                initialObject[element.trim()].push(object.name)
            }
        })
        return initialObject;
    },{})
    return obtainedData;
}

let result=groupVitamins(givenData);

console.log(result);